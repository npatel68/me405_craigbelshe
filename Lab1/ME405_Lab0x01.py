# -*- coding: utf-8 -*-
"""
@file       ME405_Lab0x01.py
@brief      An FSM for a vending machine that takes change and dispenses 
            "drinks" using the keyboard and console
@author     Craig Belshe
@date       April 21, 2021

@package                 Lab0x01
@brief                   This package provides an interface that computes change.
@author                  Craig Belshe
@date                    April 21, 2021
"""

from time import sleep
import keyboard
import FindChange

last_key = ''

def kb_cb(key):
    '''
    @brief      Adds the last key pressed to a global variable
    @details    When called, takes key and adds its name 
                (ex: 'e' for the e key) to the global variable last_key
    @param key  The key detected by keyboard.on_release_key
    '''
    
    global last_key
    last_key = key.name

def VendotronTask():
    '''
    @brief      An FSM to run a vending machine
    @details    Depending on the state, takes keyboard input from user to 
                increment user balance, allow the user to purchase drinks
                which are dispensed through notifications in the console, 
                allows the user to eject the current balance, returning the 
                exact change, displays ADs periodically, prompts the user to
                purchase another drink if they have remaining balance, and
                notifies the user if they do not have enough funds to purchase
                a drink.
    @return     YIELDS the state (between 0 and 8) that the FSM is currently in

    '''
    
    global last_key
    state = 0
    coins = ['0','1','2','3','4','5','6','7']
    coin_key = [1, 5, 10, 25, 100, 500, 1000, 2000]
    drinks = ['c','p','s','d']
    drink_key = [150, 150, 150, 150]
    drink_name = ['Cuke', 'Popsi', 'Spryte', 'Dr. Pupper']
    coin = ''
    drink = ''
    
    while True:
        
        if state == 0:
            # Initialization State
            balance = 0
            old_balance = 1
            t = 0
            print("State: ", state)
            print("Initialing...")
            state = 1
            
        elif state == 1:
            # Idle State
            if balance != old_balance:
                print("Current balance:", '$' + str(balance//100) + '.' + 
                      str(balance - 100*(balance//100)), "\nState:", state)
                old_balance = balance
            t += 1
            if last_key in coins:
                # User inserted a coin
                coin = last_key
                last_key = ''
                state = 6
            elif last_key in drinks:
                # User choose a drink
                drink = last_key
                last_key = ''
                state = 2
            elif last_key == 'e': 
                # User pressed "eject"
                last_key = ''
                state=7
            elif t > 1000:
                # Period of time has passed
                state = 8
                t = 0
                
        elif state == 2:
            #compare drink price to balance
            if drink_key[drinks.index(drink)] > balance:
                state = 4
            else:
                state = 3
                
        elif state == 3:
            # Dispense Drink
            balance -= drink_key[drinks.index(drink)]
            print('You have purchased', drink_name[drinks.index(drink)])
            
            if balance > 0:
                state = 5
            else:
                state = 0
                
        elif state == 4:
            # Error message for price > balance
            price = drink_key[drinks.index(drink)]
            print('Insufficient Funds \nPrice:', '$' + str(price//100) + '.' + 
                      str(price - 100*(price//100)))
            state = 1
            
        elif state == 5:
            # Remaining Balance after purchase
            print('Please select another drink \nRemaining Balance:',
                  '$' + str(balance//100) + '.' + 
                  str(balance - 100*(balance//100)))
            state = 1
            
        elif state == 6:
            # Add coin to balance
            balance += coin_key[coins.index(coin)]
            state = 1
            
        elif state == 7:
            # Reset balance / eject coins
            print('Your change is', FindChange.getChange(0,balance))
            balance = 0
            state = 0
            
        elif state == 8:
            # Display ad " Try Cuke Today"
            print('Try Cuke Today!!!')
            state = 1
            
        else: # Error state
            pass
        
        yield state

if __name__ == "__main__":
    
    # Check for each key needed for vending machine
    keyboard.on_release_key("e", callback=kb_cb)
    keyboard.on_release_key("c", callback=kb_cb)
    keyboard.on_release_key("p", callback=kb_cb)
    keyboard.on_release_key("s", callback=kb_cb)
    keyboard.on_release_key("d", callback=kb_cb)
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("6", callback=kb_cb)
    keyboard.on_release_key("7", callback=kb_cb)
    
    # Create instance of Generator
    vender = VendotronTask()
    
    # Repeatedly cycle through FSM until interrupt or error
    try:
        while True:
            next(vender)
            sleep(.01)

    except KeyboardInterrupt:
        print("exiting")
        
    except StopIteration:
        print("error")
        
    
    