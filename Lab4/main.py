# -*- coding: utf-8 -*-
"""
@file       main.py
@brief      Records temperature every minute and places data in datafile.csv
@details    This module uses the mcp9808.py TempSense class to find 
            ambient temperature using an external sensor in Celsius. 
            It also uses a built-in sensor to find the internal temperature 
            of the Nucleo board. It finds both temperatures every 1 minute, 
            and records them in a line in a .csv file called datafile.csv 
            along with the time since initialization. It continues to run 
            until CTRL-C is pressed, upon which is exits and saves the 
            datafile. While running, it displays taken measurements.
@author    Craig Belshe
@Date       May 12, 2021
"""

import utime, pyb
from mcp9808 import TempSense

# Initialize sensors
adcALL= pyb.ADCAll(12, 0x70000)
sensor = TempSense()
time_init = utime.ticks_ms()
time = 0


with open('datafile.csv', 'w') as file:         
    file.write('\r\n')
    
try: 
    while True:
                 
        print((time // 1000), 's')
        
        #find Internal Temperature
        v = adcALL.read_core_vref()
        temp = adcALL.read_core_temp()
        print('Internal Temp:', temp)
    
        # Find Ambient Temperature
        cel = sensor.celsius()
        print('Ambient Temperature:', cel, 'degrees Celsius')
                 
        # Append data to file
        with open('datafile.csv', 'a') as file:     
            file.write('{t},{atemp},{itemp}\r\n'.format(t=str(time), atemp=str(cel), itemp=temp))
            
        # wait 1 minute
        utime.sleep(10)
        time = utime.ticks_ms() - time_init
        
except KeyboardInterrupt:
    print('exitting')