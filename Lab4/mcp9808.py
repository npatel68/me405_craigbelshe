# -*- coding: utf-8 -*-
"""
@file       mcp9808.py
@brief      Contains Class TempSense which utilizes the external MCP9808 
            temperature sensor.
@details    The class TempSense can be used with the MCP9808 breakout board 
            to determine the ambient temperature. Upon initializing, the 
            class creates an i2c object to be used to interact with the 
            board. This file also contains test code for the class TempSense.

@author:    Craig Belshe
@date       May 12, 2021
"""

from pyb import I2C
from time import sleep
    
    
class TempSense:
    '''
    @brief      This class is used to measure temperature with the MCP9808
    @details    This class initializes by creating a I2C object which is 
    used to connect to 9808 sensor. Contains methods to measure the ambient 
    temperature in either Celsius or Fahrenheit. Can also use the 
    manufacturing ID to confirm the 9808 is connected properly.
    '''
    def __init__(self):
        self.i2c = I2C(1)
        self.i2c.init(I2C.MASTER, baudrate=400000)
        
    def check(self):
        '''
        @brief      Checks if the I2C connection to the MCP9808 board is 
                    working
        @details    Uses the manufacturing ID to check the board is connected. 
                    Returns True if so, and False if not.
        '''
        try:
            self.i2c.send(0b00000110,addr=0b0011000)
            man_id = self.i2c.recv(2, addr=0b0011000)
            # print(man_id)
            high_byte = hex(man_id[0])
            low_byte = hex(man_id[1])
            # print(high_byte)
            # print(low_byte)
            if (high_byte == '0x0') & (low_byte == '0x54'):
                return True
            else:
                return False
            
        except TimeoutError:
            return False
    
    def celsius(self):
        '''
        @brief       Returns the temperature in Celsius.
        '''
        
        # Tell Sensor at 0b0011000 to access temperature register
        self.i2c.send(0b00000101, addr=0b0011000)
        # Receive two bytes of temperature data from sensor
        temp = self.i2c.recv(2, addr=0b0011000)
        high_byte = hex(temp[0])
        low_byte = hex(temp[1])
        
        # Calculate temperature, using different method if under 0 C
        if (hex(int(high_byte, 16) & 16) == '0x10'):
            temperature = 256 - (int(high_byte[3], 16)*16 + int(low_byte, 16)/16)
        else:
            temperature = int(high_byte[3], 16)*16 + int(low_byte, 16)/16
        
        return temperature
    
    def fahrenheit(self):
        '''
        @brief     Returns the temperature in Fahrenheit.
        '''
        # Tell Sensor at 0b0011000 to access temperature register
        self.i2c.send(0b00000101, addr=0b0011000)
        # Receive two bytes of temperature data from sensor
        temp = self.i2c.recv(2, addr=0b0011000)
        high_byte = hex(temp[0])
        low_byte = hex(temp[1])
        
        # Calculate temperature, using different method if under 0 C
        if (hex(int(high_byte, 16) & 16) == '0x10'):
            temperature = 256 - (int(high_byte[3], 16)*16 + int(low_byte, 16)/16)
        else:
            temperature = int(high_byte[3], 16)*16 + int(low_byte, 16)/16
        
        return ((temperature*9/5) + 32)
            
            
if __name__ == '__main__':
    # Tests the class by running it repeatedly and printing data
    reader = TempSense()
    print(reader.check())
    
    while True:
        sleep(1)
        print(reader.celsius(), 'degrees Celsius')
        sleep(1)
        print(reader.fahrenheit(), 'degrees Fahrenheit')
        sleep(1)
        print(reader.check())
        