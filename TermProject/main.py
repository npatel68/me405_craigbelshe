from touchPanel import TouchDriver
from motor import MotorDriver as DRV8847
from controller import Ctrl
from encoder import EncoderDriver as encoder
import task_share
import cotask
import gc
import pyb

x_vals = []
theta_x_vals = []
x_dot_vals = []
theta_x_dot_vals = []
y_vals = []
theta_y_vals = []
y_dot_vals = []
theta_y_dot_vals = []


def encode_task():
    '''
    @brief A task to update the angle of the motors.
    '''
    enc = encoder()

    while True:
        enc.update()
        pos = enc.get_position()
        delta = enc.get_delta()
        queue_ang.put(pos[0])
        queue_ang.put(pos[1])
        queue_vang.put(delta[0] / .04)
        queue_vang.put(delta[1] / .04)
        yield ()


def motor_task():
    '''
    @brief A task to run the motors at the correct duty cycle.
    '''
    mot = DRV8847()
    mot.enable()
    mot.clear_fault()
    direction = True
    direction2 = True
    D = 0
    D2 = 0

    while True:
        if queue_motor1.any():
            # Get dutyccycle
            duty = queue_motor1.get()

            # Check Direction
            if duty < 0:
                D = -1 * duty
                direction = True
            else:
                D = duty
                direction = False

        if queue_motor2.any():
            duty2 = queue_motor2.get()
            if duty2 < 0:
                D2 = -1 * duty2
                direction2 = True
            else:
                D2 = duty2
                direction2 = False

        # Run the motor
        if mot.enabled:
            # pass
            mot.set_level_1(direction, D)
            mot.set_level_2(direction2, D2)
        else:
            # Clear fault if motor is disabled
            mot.clear_fault()
            print("fault cleared")

        yield ()


def touch_task():
    '''
    @brief A task to check the balls position on the touch screen.
    '''
    touch = TouchDriver()

    while True:
        data = touch.scan_ALL()
        if data[0]:
            queue_x.put(data[1])
            queue_y.put(data[2])
            print("touchpanel:", data)

        else:
            queue_x.put(0)
            queue_y.put(0)

        yield ()


def ctrl_task():
    '''
    A task to determine the needed Duty cycle according to the sensor data
    :return:
    '''
    # ctrl1 = Ctrl(k1=-.15, k2=-0.4, k3=-0.004, k4=-.1)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.1)

    # ctrl1 = Ctrl(k1=-.15, k2=-0.7, k3=-0.004, k4=-.15) # These work but have a settling time > 1 minute
    # ctrl2 = Ctrl(k1=-.15, k2=-2.1, k3=-0.004, k4=-.15)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.7, k3=-0.004, k4=-.15)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.5, k3=-0.004, k4=-.15)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.5, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.4, k3=-0.004, k4=-.08)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.2, k3=-0.004, k4=-.07)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.2, k3=-0.004, k4=-.07)

    # ctrl1 = Ctrl(k1=-.15, k2=-0.9, k3=-0.004, k4=-.11)  # Pretty okay
    # ctrl2 = Ctrl(k1=-.15, k2=-1.4, k3=-0.004, k4=-.11)

    ctrl1 = Ctrl(k1=-.15, k2=-1.6, k3=-0.004, k4=-.04)  # Works well-ish
    ctrl2 = Ctrl(k1=-.15, k2=-2.3, k3=-0.004, k4=-.04)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.1, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.3, k3=-0.004, k4=-.08)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.4, k3=-0.004, k4=-.04)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.9, k3=-0.004, k4=-.04)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.08)

    # ctrl1 = Ctrl(k1=-.15, k2=-12, k3=-0.004, k4=-0.5)
    # ctrl2 = Ctrl(k1=-.15, k2=-12.3, k3=-0.004, k4=-0.5)

    # ctrl1 = Ctrl(k1=-.15, k2=-5.5, k3=-0.004, k4=-.15)
    # ctrl2 = Ctrl(k1=-.15, k2=-5.5, k3=-0.004, k4=-.15)

    x_last = 0
    x = 0
    y = 0
    y_last = 0
    x_dot = 0
    y_dot = 0
    theta_y = 0
    theta_x = 0
    theta_y_dot = 0
    theta_x_dot = 0

    while True:
        # Get X, Y data from the queue
        if queue_x.any():
            x_last = x
            x = queue_x.get() / 1000
            x_dot = (x - x_last) / .04

        if queue_y.any():
            y_last = y
            y = queue_y.get() / 1000
            y_dot = (y - y_last) / .04

        # Get theta_x, theta_y data from the queue
        if queue_ang.any():
            theta_x = queue_ang.get() / 4000
            if queue_ang.any():
                theta_y = queue_ang.get() / 4000

        if queue_vang.any():
            theta_x_dot = queue_vang.get() / 4000
            if queue_vang.any():
                theta_y_dot = queue_vang.get() / 4000

        # if queue_end.any():
        #     print('ball is off the board')
        #     q = queue_end.get()

        # else:
        # Find the duty cycle for each motor
        # print('ctrl data:', x, theta_x, x_dot, theta_x_dot)
        x_vals.append(x * 1000)
        theta_x_vals.append(theta_x * 4000)
        x_dot_vals.append(x_dot * 1000)
        theta_x_dot_vals.append(theta_x_dot * 4000)
        D1 = ctrl1.DutyCycle(x, theta_x, x_dot, theta_x_dot)
        # print('ctrl data:', y, theta_y, y_dot, theta_y_dot)
        y_vals.append(y * 1000)
        theta_y_vals.append(theta_y * 4000)
        y_dot_vals.append(y_dot * 1000)
        theta_y_dot_vals.append(theta_y_dot * 4000)
        D2 = ctrl2.DutyCycle(y, theta_y, y_dot, theta_y_dot)

        # print('duty:', D1, D2)
        queue_motor1.put(D1)
        queue_motor2.put(D2)
        yield ()


def data_task():
    '''
    A task responsible for collecting data and storing it in appropriate format.
    '''
    # Writes the captured data into a CSV file
    # We created separate CSV files to capture different values as that way
    # we could capture more values without overflowing the memory.
    file1 = open('yDotData.csv', 'w')
    for n in range(len(x_vals)):
        line = "%f, %f\n" % (n,
                             # x_vals[n],
                             # theta_x_vals[n],
                             # x_dot_vals[n],
                             # theta_x_dot_vals[n],
                             # y_vals[n],
                             # theta_y_vals[n],
                             y_dot_vals[n],
                             # theta_y_dot_vals[n]
                             )
        file1.write(line)
    file1.close()


def UI_task():
    '''
    A task responsible for interaction between the user and the program.
    :return:
    '''
    # Run the scheduler with the chosen scheduling algorithm as soon as a character
    # is sent through the serial port.
    vcp = pyb.USB_VCP()
    while not vcp.any():
        continue

    # Quit if any character is sent through the serial port.
    while not vcp.any():
        cotask.task_list.pri_sched()

    # Empty the comm port buffer of the character(s) just pressed
    vcp.read()


if __name__ == '__main__':

    # Create Queues to share data between each task.
    queue_x = task_share.Queue('f', 1, thread_protect=False, overwrite=True,
                               name="Queue_0")
    queue_y = task_share.Queue('f', 1, thread_protect=False, overwrite=True,
                               name="Queue_1")
    queue_ang = task_share.Queue('f', 2, thread_protect=False, overwrite=True,
                                 name="Queue_2")
    queue_vang = task_share.Queue('f', 2, thread_protect=False, overwrite=True,
                                  name="Queue_3")
    queue_motor1 = task_share.Queue('f', 1, thread_protect=False, overwrite=True,
                                    name="Queue_4")
    queue_motor2 = task_share.Queue('f', 1, thread_protect=False, overwrite=True,
                                    name="Queue_5")
    queue_end = task_share.Queue('f', 1, thread_protect=False, overwrite=True,
                                 name="Queue_6")

    # Instantiate each task
    task_enc = cotask.Task(encode_task, name='Task_enc', priority=3,
                           period=40, profile=True, trace=False)
    task_mot = cotask.Task(motor_task, name='Task_mot', priority=2,
                           period=15, profile=True, trace=False)
    task_tp = cotask.Task(touch_task, name='Task_tp', priority=3,
                          period=40, profile=True, trace=False)
    task_ctrl = cotask.Task(ctrl_task, name='Task_ctrl', priority=2,
                            period=15, profile=True, trace=False)

    cotask.task_list.append(task_enc)
    cotask.task_list.append(task_mot)
    cotask.task_list.append(task_tp)
    cotask.task_list.append(task_ctrl)

    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect()

    debug_count = 0
    while debug_count != 2000:
        cotask.task_list.pri_sched()
        debug_count += 1

    # Unfortunately, we couldn't get the UI to work in time the way we wanted so we commented it
    # out so that functionality remains. The program starts as soon as you run it as opposed
    # to how we wanted it to run on a key press.
    #    UI_task()
    data_task()

    print ('\n' + str(cotask.task_list) + '\n')
    print (task_share.show_all())
    #    print (task1.get_trace ())
    print ('\r\n')