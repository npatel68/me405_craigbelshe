# -*- coding: utf-8 -*-
"""
@file       TouchPanel.py     
@brief      Drivers for receiving data from the Resistive Touch Panel
@details    This file contains the class TouchDriver which on initialization 
            takes the pins connected to the touch panel, as well as the 
            dimensions of the panel. The class then provides 4 methods related 
            to the panel. scan_Y() and scan_X() return the Y and X coord., 
            respectively. scan_Z() determines if the panel is being touched. 
            scan_ALL() performs all 3 and returns (Z, X, Y)
            
@date May 16, 2021
@author: Craig Belshe
"""

import pyb
import utime
#import time
import math

class TouchDriver:
    '''
    @brief      A driver to allow for interaction with  Resistive touch panel
    @details    This class defines methods to interact with a touch panel - measuring the X and Y position of a touch,
                as well as determining if anything is touching the panel.
    '''
       
    def __init__(self, ym=pyb.Pin.board.PA0, xm=pyb.Pin.board.PA1,
                 yp=pyb.Pin.board.PA6, xp=pyb.Pin.board.PA7,
                 length=100, width=176):
        '''
        @brief Initializes the TouchPanel object with the correct pins and measurements.
        @details
            @param ym       the the pyb.pin for the minus y (y-) wire from the panel (default: pyb.Pin.board.PA0)
            @param xm       the the pyb.pin for the minus x (x-) wire from the panel (default: pyb.Pin.board.PA1)
            @param yp       the the pyb.pin for the plus y (y+) wire from the panel (default: pyb.Pin.board.PA6)
            @param xp       the the pyb.pin for the plus x (x+) wire from the panel (default: pyb.Pin.board.PA7)
            @param length:  the y-direction length of the board (default: 100mm)
            @param width:   the x-direction width of the board (default: 176mm
        '''
        self.ym = ym
        self.xm = xm
        self.yp = yp
        self.xp = xp
        self.length = length
        self.width = width
    
    def scan_X(self):
        '''
        @brief      Scans the touch panel for the X value of the touch in mm relative to the center
        @returns    x value in mm
        '''
        # Set x pins to out
        self.xm.init(pyb.Pin.OUT_PP)
        self.xp.init(pyb.Pin.OUT_PP)
        
        self.xm.value(False) # xm is 0 V
        self.xp.value(True) # xp is 3.3 V
        
        # Set y pins to in
        adc = pyb.ADC(self.yp)
        self.ym.init(pyb.Pin.IN)
        
        #wait x amount of time
        utime.sleep_us(10)
        
        # Measure yp
        Vx = adc.read()
        return ((Vx / 4095 * self.width) - self.width/2) # x-pos in mm
    
    def scan_Y(self):
        '''
        @brief      Scans the touch panel for the Y value of the touch in mm relative to the center
        @returns    y value in mm
        '''
        # Set y pins to out
        self.ym.init(pyb.Pin.OUT_PP)
        self.yp.init(pyb.Pin.OUT_PP)
        
        self.ym.value(False) # ym is 0 V
        self.yp.value(True) # yp is 3.3 V
        
        # Set x pins to in
        adc = pyb.ADC(self.xp)
        self.xm.init(pyb.Pin.ANALOG)
        
        utime.sleep_us(10)
        
        # Measure xp
        Vy = adc.read()
        return -((Vy / 4095 * self.length) - self.length/2) # y-pos in mm
    
    def scan_Z(self):
        '''
        @brief      Scans the touch panel to check if it detects anything
        @returns    boolean, True if object is detected
        '''
        self.xm.init(pyb.Pin.OUT_PP)
        self.yp.init(pyb.Pin.OUT_PP)
        
        self.xm.value(False) # xm is 0 V
        self.yp.value(True) # yp is 3.3 V
        
        adc = pyb.ADC(self.xp) # read xp
        self.ym.init(pyb.Pin.ANALOG) # float ym
        
        utime.sleep_us(10)
        #time.sleep(.01)

        if adc.read() < 10:
            return False
        
        return True
    
    def scan_ALL(self):
        '''
        @brief      Scans the touch panel for all measurements
        @returns    (Z,X,Y)
        '''
        return (self.scan_Z(), self.scan_X(), self.scan_Y())


if __name__ == '__main__':
    '''
    @brief  Some simple tests for the touch panel driver
    '''
    # Test class
    panel = TouchDriver()
    start = utime.ticks_us()
    list = [None]*50
    for i in range(50): # Make 50 measurements

        list[i] = panel.scan_ALL()
        #utime.sleep_ms(10)

    end = utime.ticks_us()
    diff = utime.ticks_diff(end, start)
    # print(list)
    # avg = sum(list)/len(list)
    # var = 0  # Find Variance
    # for i in list:
    #     var += (i - avg)**2
    # var = var / len(list)
    # stDev = math.sqrt(var)  # Find Standard Deviation
    # print('stdev:', stDev, "mean:", avg, 'maxVariation', max(list)-min(list))
    print("Total time:", diff, '\n Time per run:', diff/50)

        
    


    
    